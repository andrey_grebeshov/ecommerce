import GoogleProvider from 'next-auth/providers/google'
import NextAuth from 'next-auth'

console.log('process.env.GOOGLE_ID', process.env.GOOGLE_ID);


export default NextAuth({
	providers: [
		GoogleProvider({
			clientId: process.env.GOOGLE_ID || '',
			clientSecret: process.env.GOOGLE_SECRET || ''
		})
	]
})